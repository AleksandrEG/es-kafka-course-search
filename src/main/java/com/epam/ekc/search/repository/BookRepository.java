package com.epam.ekc.search.repository;

import com.epam.ekc.search.dto.Resource;
import com.epam.ekc.search.model.Book;
import org.springframework.stereotype.Repository;

@Repository
public class BookRepository extends ElasticRepository<Book, Resource> {

}
