package com.epam.ekc.search.dto;

import com.epam.ekc.search.model.Author;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.CloseableThreadContext.Instance;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Resource {

  private String bookId;

  private String bookType;

  private String genre;

  private String title;

  private Set<Author> authors;

}
