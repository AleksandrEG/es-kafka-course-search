package com.epam.ekc.search.service;

import com.epam.ekc.search.dto.Resource;
import com.epam.ekc.search.repository.BookRepository;
import java.io.IOException;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

@Service
@RequiredArgsConstructor
public class ResourceService {

  private final BookRepository bookRepository;

  public List<Resource> getResources(String query,
                                     MultiValueMap<String, Object> facets,
                                     String customerId) {
    return bookRepository.getResources(query, facets, customerId);
  }

  public List<String> autoComplete(String query, String customerId) throws IOException {
    return bookRepository.autocomplete(query, customerId);
  }
  
}
